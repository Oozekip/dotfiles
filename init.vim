function! BuildYCM(info)
  if a:info.status == 'installed' || a:info.force
    !./install.sh --clang-completer
  endif
endfunction

call plug#begin('~/.vim/plugged')

Plug 'rust-lang/rust.vim'
Plug 'lyuts/vim-rtags'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'airblade/vim-gitgutter'
Plug 'neomake/neomake'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM')}
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable'}
Plug 'zchee/deoplete-clang'
Plug 'rhysd/vim-clang-format'
Plug 'kana/vim-operator-user'

call plug#end()

set number
set rnu

autocmd InsertEnter * :set norelativenumber
autocmd InsertLeave * :set relativenumber 

" Rust specific setting: Disable meddling with tab sizes
"let g:rust_recommended_style = 0

  " Replace tabs with 2 spaces
set tabstop=2 shiftwidth=2 expandtab

  " Auto intenting
set autoindent
set smartindent
set cindent

  " Column marker
set colorcolumn=81
" Folds
set foldmethod=syntax 
"set foldnestmax=1 
set foldlevelstart=99

  " Misc
set showcmd

  "Clipboard
set clipboard=unnamedplus

  " Splits
set splitbelow
set splitright

"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>

" Doxygen settings
let g:DoxygenToolkit_briefTag_pre = "\\brief "
let g:DoxygenToolkit_templateParamTag_pre = "\\tparam "
let g:DoxygenToolkit_paramTag_pre = "\\param "
let g:DoxygenToolkit_returnTag = "\\return "
let g:DoxygenToolkit_throwTag_pre = "\\throw " " @exception is also valid
let g:DoxygenToolkit_fileTag = "\\file "
let g:DoxygenToolkit_authorTag = "\\author "
let g:DoxygenToolkit_dateTag = "\\date "
let g:DoxygenToolkit_versionTag = "\\version "
let g:DoxygenToolkit_blockTag = "\\name "
let g:DoxygenToolkit_classTag = "\\class "

" Gitgutter
set signcolumn=yes

"YCM settings
let g:ycm_confirm_extra_conf = 0
set completeopt-=preview

set ignorecase
set smartcase
set gdefault

function! NumberToggle()
  if(&relativenumber == 1)
    set nornu
    set number
  else
    set rnu
  endif
endfunc

noremap <leader>r :call NumberToggle()<cr>
noremap <C-q> :q<cr>

"Use ; for commands
noremap ; :
if has('nvim')
    set guicursor=
    tnoremap <Esc> <C-\><C-n>
    tnoremap <A-h> <C-\><C-N><C-w>h
    tnoremap <A-j> <C-\><C-N><C-w>j
    tnoremap <A-k> <C-\><C-N><C-w>k
    tnoremap <A-l> <C-\><C-N><C-w>l
    inoremap <A-h> <C-\><C-N><C-w>h
    inoremap <A-j> <C-\><C-N><C-w>j
    inoremap <A-k> <C-\><C-N><C-w>k
    inoremap <A-l> <C-\><C-N><C-w>l
    nnoremap <A-h> <C-w>h
    nnoremap <A-j> <C-w>j
    nnoremap <A-k> <C-w>k
    nnoremap <A-l> <C-w>l
    nnoremap <leader>t :sp \| res 15 \| term<cr>
    tnoremap <C-q> <C-\><C-n> :q<cr>
endif


" Airline
set encoding=utf-8
if !(has("win32"))
  let g:airline_powerline_fonts = 1
endif

" NERDTree
let g:airline#extensions#syntastic#enabled = 1
let g:airline_theme= 'gruvbox'
"let g:airline_theme= 'bubblegum'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI=1
let NERDTreeHighlightCursorLine=1
"let NERDTreeQuitOnOpen=1
set updatetime=250

" Neomake
autocmd! BufWritePost * Neomake
"let g:neomake_cpp_enabled_makers = ['clang']
"let g:neomake_cpp_clang_maker = {
"   \ 'exe': 'clang++',
"   \ 'args': ['-std=c++17', '-Wall', '-Wextra', '-Wpedantic', '-Wno-sign-conversion'],
"   \ }
let g:neomake_cpp_enabled_makers = ['clangtidy']
let g:neomake_cpp_clangtidy_maker = {
   \ 'exe': '/usr/bin/clang-tidy',
   \ 'args': ['-checks=*, -cppcoreguidelines-pro-type-union-access'],
   \}

" Gruvbox
set background=dark
"set background=light
"let g:gruvbox_contrast_light="hard"
let g:gruvbox_italic=1
let g:gruvbox_invert_signs=0
let g:gruvbox_improved_strings=0
let g:gruvbox_improved_warnings=1
let g:gruvbox_undercurl=1
let g:gruvbox_contrast_dark="hard"
colorscheme gruvbox

" Deoplete
let g:deoplete#enable_at_startup=1
let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-6.0/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/include/clang'
" C or C++ standard version
let g:deoplete#sources#clang#std#c = 'c11'
" or c++
let g:deoplete#sources#clang#std#cpp = 'c++17'

" Let <Tab> also do completion
inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ deoplete#mappings#manual_complete()
function! s:check_back_space() abort "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" Enter: complete&close popup if visible (so next Enter works); else: break undo
inoremap <silent><expr> <Cr> pumvisible() ?
            \ deoplete#mappings#close_popup() : "<C-g>u<Cr>"

" Ctrl-Space: summon FULL (synced) autocompletion
inoremap <silent><expr> <C-Space> deoplete#mappings#manual_complete()

" Escape: exit autocompletion, go to Normal mode
inoremap <silent><expr> <Esc> pumvisible() ? "<C-e><Esc>" : "<Esc>"

let g:clang_format#code_style="llvm"
let g:clang_format#auto_format=1
